$(document).ready(function(){
    // Product list page
    var productWrapper = $('#products-list');
    if ( productWrapper.length ) {
        var page = 1;

        var pagination = productWrapper.find('.pagination');
        var deleteBtn = productWrapper.find('#delete');
        var productsList = productWrapper.find('.product-list');

        // On load and on hash change - change page
        $(window).on('load hashchange', function(){
            page = window.location.hash;
            if ( page ) {
                page = page.substr(1);
            } else {
                page = 1;
            }

            $.ajax({
                url: 'inc/ajax.php',
                method: 'POST',
                data: {
                    function: 'productByPage',
                    page: page,
                },
                dataType: 'HTML',
                success: function(data) {
                    productsList.html(data);
                }
            });
            $.ajax({
                url: 'inc/ajax.php',
                method: 'POST',
                data: {
                    function: 'pagination',
                    page: page,
                },
                dataType: 'HTML',
                success: function(data) {
                    pagination.html(data);
                    pagnationBtn();
                }
            });
        });

        function pagnationBtn () {
            pagination.find('button').on('click', function(){
                page = $(this).attr('data-page');
                window.location.hash = page;
            });
        }

        pagnationBtn();

        deleteBtn.on('click', function() {
            var products = [];
            productsList.find('.product input:checked').each(function(){
                var id = $(this).closest('.product').attr('data-id');
                products.push(id);
            });
            if ( products.length ) {
                $.ajax({
                    url: 'inc/ajax.php',
                    method: 'POST',
                    data: {
                        function: 'deleteProducts',
                        products: products,
                        page: page,
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        if ( data.status ) {
                            deleteBtn.next('.message').slideDown().children('p').text('Products deleted');
                            window.location.hash = data.page;
                        } else {
                            deleteBtn.next('.message').slideDown().children('p').text('Oops something went wrong!');
                        }
                    }
                });
            } else {
                deleteBtn.next('.message').slideDown().children('p').text('Please select at least one product!');
            }
        });
    }

    // Add product form
    var form = $('#add-product');
    if ( form.length ) {
        // Variables
        var product, sku, name, price, type, value;
        product = {
            sku:"",
            name:"",
            price:"",
            type:"",
            value:"",
        }
        sku = form.find('#sku');
        name = form.find('#name');
        price = form.find('#price');
        type = form.find('#type');
        value = "";
        // Set value depending on selected type
        function setType () {
            switch (type.val()) {
                case 'size':
                    value = form.find('#size').val() + ' MB';
                break;
                case 'weight':
                    value = form.find('#weight').val() + ' Kg';
                break;
                case 'dimension':
                    value = form.find('#height').val() + 'x' + form.find('#width').val() + 'x' + form.find('#length').val();
                break;
            }
        }
        // Clear form data
        function clearFormData () {
            sku.val('');
            name.val('');
            price.val('');
            type.val('').trigger('change');
        }
        // Type changing
        type.on('change', function(){
            $.ajax({
                url: 'inc/ajax.php',
                method: 'POST',
                data: {
                    function: 'typeSwitcher',
                    type: type.val(),
                },
                dataType: 'HTML',
                success: function(data) {
                    form.find('.type-wrapper').html(data);
                }
            });
        });

        // Validation
        function validateForm (e) {
            e.preventDefault();
            // Add data to JSON
            setType();
            product.sku = sku.val();
            product.name = name.val();
            product.price = price.val() + ' $';
            product.type = type.find('option:selected').text();
            product.value = value;
            $.ajax({
                url: 'inc/ajax.php',
                method: 'POST',
                data: {
                    function: 'addProduct',
                    product: JSON.stringify(product),
                },
                dataType: 'JSON',
                success: function(data) {
                    if ( data.valid ) {
                        sku.removeClass('error');
                        sku.next('.error-message').slideUp();
                        form.find('.message').slideDown().children('p').text(data.message);
                        clearFormData();
                    } else {
                        sku.addClass('error');
                        sku.next('.error-message').children('p').text(data.message);
                        sku.next('.error-message').slideDown();
                        form.find('.message').slideUp();
                    }
                }
            });
        }
        // On submit
        form.on('submit',validateForm);
    }
});
