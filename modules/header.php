<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page_title; ?></title>
    <link rel="stylesheet" href="/assets/css/style.css">
</head>
<body>
<header id="header">
    <div class="container">
        <div class="flexbox h-padd sb vc">
            <div class="col">
                <a class="link logo-wrapper" href="/">
                    <p>LOGO</p>
                </a>
            </div>
            <div class="col">
                <ul class="menu">
                    <li>
                        <a class="link menu-item" href="/">Products</a>
                        <a class="link menu-item" href="/add.php">Add</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
