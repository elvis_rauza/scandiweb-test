-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2019 at 07:41 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ID` bigint(20) NOT NULL,
  `SKU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `type` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ID`, `SKU`, `name`, `price`, `type`, `value`) VALUES
(5, 'JVC200110', 'Acme DISC', '1.00', 'Size', '128 MB'),
(6, 'JVC200111', 'Acme DISC', '2.00', 'Size', '256 MB'),
(7, 'JVC200112', 'Acme DISC', '4.00', 'Size', '512 MB'),
(8, 'JVC200113', 'Acme DISC', '8.00', 'Size', '1024 MB'),
(19, 'TR120550', 'Chair', '40.00', 'Dimension', '24x45x15'),
(20, 'TR120551', 'Chair', '40.00', 'Dimension', '24x45x15'),
(21, 'TR120552', 'Chair', '40.00', 'Dimension', '24x45x15'),
(22, 'TR120553', 'Chair', '40.00', 'Dimension', '24x45x15'),
(23, 'GGWP0000', 'War and Peace', '20.00', 'Weight', '2 Kg'),
(24, 'GGWP0001', 'War and Peace', '20.00', 'Weight', '2 Kg'),
(25, 'GGWP0002', 'War and Peace', '20.00', 'Weight', '2 Kg'),
(26, 'GGWP0003', 'War and Peace', '20.00', 'Weight', '2 Kg'),
(46, 'JVC200115', 'Acme DISC', '10.00', 'Size', '128 MB');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `SKU` (`SKU`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
