<?php
// Only process POST reqeusts.
if ($_SERVER["REQUEST_METHOD"] == "POST") :
    include_once('form.php');
    include_once('product.php');
    include_once('functions.php');
    include_once('pagination.php');

    $product = new Product();
    $functions = new Functions();

    if ( isset($_POST['function']) && !empty($_POST['function']) ) :
        $function = $_POST['function'];
        switch ($function) :
            // Product type handling
            case 'typeSwitcher':
                if ( isset($_POST['type']) && !empty($_POST['type']) ) :
                    $type = $_POST['type'];
                    $form = new Form();
                    switch ($type) :
                        case 'size':
                            $data = $form->size();
                            echo $data;
                        break;

                        case 'weight':
                            $data = $form->weight();
                            echo $data;
                        break;

                        case 'dimension':
                            $data = $form->dimension();
                            echo $data;
                        break;

                        default:
                            echo "";
                    endswitch;
                endif;
            break;

            // Product add
            case 'addProduct':
                if ( isset($_POST['product']) && !empty($_POST['product']) ) :
                    $data = array(
                        'valid' => false,
                        'message' => '',
                    );
                    $product_data = $_POST['product'];
                    $product_data = json_decode($product_data);

                    $sku = $product_data->sku;
                    $name = $product_data->name;
                    $price = $product_data->price;
                    $type = $product_data->type;
                    $value = $product_data->value;


                    $dbProduct = $product->getProductbySKU($sku);
                    $valid_sku = $functions->validateSKU($sku);

                    if ( $dbProduct == $sku ) :
                        $data['valid'] = false;
                        $data['message'] = "SKU already exists";
                    elseif ( !$valid_sku ) :
                        $data['valid'] = false;
                        $data['message'] = "Please enter valid SKU.";
                    else :
                        $product->addProduct($sku, $name, $price, $type, $value);
                        $data['valid'] = true;
                        $data['message'] = "Product added.";
                    endif;

                    $return = json_encode($data);
                    echo $return;
                endif;
            break;

            // Product list
            case 'productByPage':
                if ( isset($_POST['page']) && !empty($_POST['page']) ) :
                    $page = $_POST['page'];
                    $products = $functions->getProductsbyPage($page);
                    foreach ($products as $post) :
                        $single_product = $product->singleProduct($post);
                        echo $single_product;
                    endforeach;
                endif;
            break;

            // Pagination
            case 'pagination':
                if ( isset($_POST['page']) && !empty($_POST['page']) ) :
                    $page = $_POST['page'];
                    $pagination = new Pagination;
                    $total_pages = $functions->getPageCount();
                    $pagination->menu($page, $total_pages);
                endif;
            break;

            // Delete products
            case 'deleteProducts':
                if ( isset($_POST['products']) && !empty($_POST['products']) ) :
                    $data = array();
                    $currentPage = $_POST['page'];
                    $products = $_POST['products'];
                    foreach ( $products as $sku ) :
                        $sucess = $product->deleteProduct($sku);
                        $sucess = true;
                        if ( $sucess ) :
                            $data['status'] = true;
                        else:
                            $data['status'] = false;
                        endif;
                    endforeach;

                    $lastPage = $functions->getPageCount();
                    if ( $currentPage > $lastPage && $currentPage != 1 ) :
                        $data['page'] = $currentPage - 1;
                    else:
                        $data['page'] = $currentPage;
                    endif;

                    $response = json_encode($data);
                    echo $response;
                endif;
            break;

            default:

            break;
        endswitch;
    endif;
else :
    http_response_code(403);
    echo "Forbidden";
endif;
?>
