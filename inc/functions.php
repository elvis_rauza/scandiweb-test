<?php
include_once('database.php');
/**
 * Add, get and delete product from DB
 */
class Functions {
    public function getAllProducts() {
        $instance = ConnectDb::getInstance();
        $conn = $instance->getConnection();
        $sql = "SELECT * FROM products";
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getPageCount() {
        $instance = ConnectDb::getInstance();
        $conn = $instance->getConnection();
        $sql = "SELECT COUNT(ID) FROM products";
        $stmt = $conn->query($sql);
        $rows = $stmt->fetch();
        $total_rows = $rows[0];
        $rpp = 12;

        $last = ceil($total_rows / $rpp);
        if ( $last < 1 ) {
            $last = 1;
        }
        return $last;
    }

    public function getProductsbyPage($page) {
        $limit = 12;
        $page = $page - 1;
        $start_from = $page * $limit;
        $instance = ConnectDb::getInstance();
        $conn = $instance->getConnection();
        $sql = "SELECT * FROM products LIMIT $start_from, $limit";
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll();
        return $result;
    }

    public function validateSKU($sku) {
        if (preg_match('/[A-Z][A-Z]([A-Z]|\d)([A-Z]|\d)\d+/', $sku)) {
            return true;
        } else {
            return false;
        }
    }
}
