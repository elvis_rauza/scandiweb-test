<?php
/**
 * Paginnation
 */
class Pagination {
    public function menu($currentPage,$totalPages) { ?>
        <ul class="pages">
            <?php
            if ( $totalPages > 1 ) :
                for ($i = 1; $i <= $totalPages; $i++) :
                    if ( $currentPage == $i ) :
                        $data = '
                        <li class="page current">
                            <p><?php echo $i; ?></p>
                        </li>';
                    else: ?>
                        <li class="page">
                            <button type="button" name="page" data-page="<?php echo $i; ?>"><?php echo $i; ?></button>
                        </li>
                        <?php
                    endif;
                endfor;
            else: ?>
                <li>
                    <p>There is only one page</p>
                </li>
                <?php
            endif; ?>
        </ul>
        <?php
    }
}
?>
