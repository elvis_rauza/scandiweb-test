<?php
include_once('database.php');
/**
 * Product
 */
class Product {

    public function addProduct($sku, $name,$price,$type,$value) {
        $pdo = $this->connect();
        $sql = "INSERT INTO products SET sku=?, name=?, price=?, type=?, value=?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$sku, $name, $price, $type, $value]);
        return true;
    }

    public function deleteProduct($sku) {
        $pdo = $this->connect();
        $sql = "DELETE FROM products WHERE SKU=?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$sku]);
        return true;
    }

    public function getProductbySKU($sku) {
        $pdo = $this->connect();
        $sql = "SELECT * FROM products WHERE SKU=?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$sku]);
        $result = $stmt->fetchAll();
        foreach ( $result as $data ) :
            return $data['SKU'];
        endforeach;
    }

    public function singleProduct($post) {
        $data = '
        <div class="col col-25">
            <div class="product" data-id="'. $post['SKU'] .'">
                <p>'. $post['SKU'] .'</p>
                <h3>'. $post['name'] .'</h3>
                <p>'. $post['price'] .' $</p>
                <p>'. $post['type'] .': '. $post['value'].'</p>
                <div class="checkbox-wrapper">
                    <input type="checkbox" name="" value="">
                    <div class="checkbox"></div>
                </div>
            </div>
        </div>';
        echo $data;
    }
}
