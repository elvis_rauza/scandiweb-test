<?php
/**
 * Display different field depending on type
 */
class Form {
    public function size() { ?>
        <div class="col col-full">
            <label for="size">Size <span>*</span></label>
            <input class="input" id="size" type="number" name="size" value="" placeholder="512 MB" required min="0" step="1">
        </div>
        <?php
    }
    public function weight() { ?>
        <div class="col col-full">
            <label for="weight">Weight <span>*</span></label>
            <input class="input" id="weight" type="number" name="weight" value="" placeholder="10 Kg" required min="0" step="0.001">
        </div>
        <?php
    }
    public function dimension() { ?>
        <div class="col col-33">
            <label for="height">Height <span>*</span></label>
            <input class="input" id="height" type="text" name="height" value="" placeholder="10" required min="0" step="0.01">
        </div>
        <div class="col col-33">
            <label for="width">Width <span>*</span></label>
            <input class="input" id="width" type="text" name="width" value="" placeholder="10" required min="0" step="0.01">
        </div>
        <div class="col col-33">
            <label for="length">Length <span>*</span></label>
            <input class="input" id="length" type="text" name="length" value="" placeholder="10" required min="0" step="0.01">
        </div>
        <?php
    }
}
?>
