<?php
$page_title = "Product List";
include_once('modules/header.php');
include_once('inc/product.php');
include_once('inc/functions.php');
$functions = new Functions; ?>

<section class="main" id="products-list">
    <div class="container">
        <div class="header">
            <div class="flexbox full he">
                <div class="content">
                    <button class="btn btn-red" id="delete" type="button" name="button">Delete selected</button>
                    <div class="col col-full message">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-list flexbox hc">
            <?php
            $page = 1;
            $products = $functions->getProductsbyPage($page);
            foreach ($products as $post) :
                $product = new Product;
                echo $product->singleProduct($post);
            endforeach; ?>
        </div>
        <div class="pagination"></div>
    </div>
</section>
<?php
include_once('modules/footer.php');
?>
