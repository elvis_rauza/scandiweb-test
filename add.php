<?php
$page_title = "Product Add";
include_once('modules/header.php'); ?>
<section class="main">
    <div class="container">
        <form class="form" id="add-product" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
            <div class="flexbox">
                <div class="col col-full">
                    <h1 class="title"><?php echo $page_title; ?></h1>
                </div>
                <div class="col col-5">
                    <label for="sku">SKU <span>*</span></label>
                    <input class="input" id="sku" type="text" name="sku" value="" placeholder="JCV200123" required>
                    <div class="error-message">
                        <p></p>
                    </div>
                </div>
                <div class="col col-5">
                    <label for="name">Product name <span>*</span></label>
                    <input class="input" id="name" type="text" name="product-name" value="" placeholder="Acem DISC" required>
                </div>
                <div class="col col-5">
                    <label for="price">Price <span>*</span></label>
                    <input class="input" id="price" type="number" name="price" value="" placeholder="20.00 $" min="0" step="0.01" required>
                </div>
                <div class="col col-5">
                    <label for="type">Type <span>*</span></label>
                    <select class="input" id="type" name="type" required>
                        <option value="">Choose type</option>
                        <option value="size">Size</option>
                        <option value="weight">Weight</option>
                        <option value="dimension">Dimension</option>
                    </select>
                </div>
            </div>
            <div class="type-wrapper flexbox">
            </div>
            <div class="flexbox hc">
                <div class="col">
                    <div class="message">
                        <p></p>
                    </div>
                    <button class="btn btn-green" id="submit" type="submit" name="button">Add Product</button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
include_once('modules/footer.php');
